# Development Q

This repository contains scripts, tools and documentation for the development of Q.

## Docker Environment

The entire Q Docker environment can be created with Docker Compose. To use Docker Compose you first have to create the `secrets.env`-file in the `docker-environment` folder. Then just `cd` to the `docker-environment` folder and run `docker-compose up`. A MongoDB database and all microservices should now start in Docker Containers.  
To run the environment in the background run `docker-compose up -d`, stop the environment with `docker-compose stop`.  
To update all docker images run `docker-compose pull` when the environment is stopped.

### Services and Docker Ports

Each service should be accessed through the gateway, but for development purposes, it is also exposed through a unique port. These ports are only exposed to localhost, so you can not access them through your network or the internet if your host is public.
**Note:** These ports are exposed through docker and are not internal service ports. Each service in its container should be listening on port 3000.

| **Service** | **Port** |
| ------ | ------ |
| [frontend](https://gitlab.com/project-q/q-frontend) | 8080 |
| [gateway](https://gitlab.com/project-q/q-gateway) | 4000 |
| [product](https://gitlab.com/project-q/q-product) | 3000 |
| [product-template](https://gitlab.com/project-q/q-producttemplate) | 3001 |
| [shopping-cart](https://gitlab.com/project-q/q-shoppingcart) | 3002 |
| [order](https://gitlab.com/project-q/q-order) | 3003 |
| [account](https://gitlab.com/project-q/q-account) | 3004 |
| [auth](https://gitlab.com/project-q/q-auth) | 3005 |
| [media](https://gitlab.com/project-q/q-media) | 3006 |
| [image-recognition](https://gitlab.com/project-q/q-imagerecognition) | 3007 |

## GitLab CI Templates

The `gitlab-ci-templates` folder contains templates to use for GitLab CI in projects.  
To use docker build and release stages for projects create a `.gitlab-ci.yml` file with the following contents:

``` yml
include:
  - 'https://gitlab.com/project-q/development/raw/master/gitlab-ci-templates/docker.yml'
```

## Documentation

Documentation for the entire Project Q can be found in [the documentation project wiki](https://gitlab.com/project-q/documentation/wikis/home).
